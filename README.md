### OneMillionWallet

This is the wallet that we submit for the OMW Hackathon.

Here is the presentation video : TDB

Here is the live version of the wallet : https://onemillionwallets.aurelienfoucault.fr/

The main features of the wallets are :
- All blockchain available in Covalent API are available and you can change dynamically in navbar.
- All currencies available in Covalent API are available and you can change dynamically in navbar.
- Dark mode.
- Can change the address of visualization at any time.
- All user preferences recorded for next sessions.
- Graph with value of the whole portfolio using Covalent API.
- Table with all coins, balance, price, price evolution using Covalent API and CoinGecko API to complete the prices.
- Click on coins to see informations about the coin for the user : value over time, transactions.
- NFT menu.
- Click on + button to see NFT as slideshow.
- NFT have special parsing and it's easy to add a special parsing in the future (See Add NFT parsing section)

## How to add a parsing of a new NFT collection

Create a folder in `src/NFT` like the `Default` one. Code the style of the overlay on the slide and code the style of the popup which appears on the slide.

Import your files at the top of `src/nft_visualize.js` like the other one.

Add your components in the switch in the `SlideContent()` and `PopupContent()` of the `src/nft_visualize.js` file.

And TADA ! You have made a new custon parsing. Easy no ?