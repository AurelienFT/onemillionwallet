import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import NftVisualize from './nft_visualize';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import reportWebVitals from './reportWebVitals';
import Coin from './coin'
import NftSupport from './nft_support';

ReactDOM.render(
  <React.StrictMode>
    <Router>
					<Switch>
						<Route component={App} exact path="/" />
						<Route component={NftVisualize} exact path="/nft/:mainnet/:address/:symbol" />
						<Route component={Coin} exact path="/coin/:mainnet/:symbol/:address" />
						<Route component={NftSupport} exact path="/nft_support" />
					</Switch>
				</Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
