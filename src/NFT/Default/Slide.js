import './slide.scss';

function Slide({nft}) {
  return (
    <div>
      <h2 className="slideTitle">{nft.external_data.name}</h2>
      <p className="slideDescription">{nft.external_data.description}</p>
    </div>
  );
}

export default Slide;