function Popup(props) {
  return (
    <div
      className="fixed z-10 inset-0 overflow-y-auto"
      aria-labelledby="modal-title"
      role="dialog"
      aria-modal="true"
    >
      <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center xl:block sm:p-0">
        <div
          className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"
          aria-hidden="true"
        ></div>
        <span
          className="hidden xl:inline-block sm:align-middle xl:h-screen"
          aria-hidden="true"
        >
          &#8203;
        </span>

        <div className="inline-block align-bottom rounded-xl bg-white text-left overflow-hidden shadow-xl transform transition-all xl:my-8 xl:align-middle xl:max-w-2xl xl:w-full">
          <div className="grid grid-cols-5 min-w-full">
            <div className="col-span-3 w-full">
              <img
                className="w-full max-w-full min-w-full"
                src={props.modalData.external_data.image}
                alt="Description"
              />
            </div>

            <div className="col-span-2 relative pl-4 text-black">
              <header className="border-b border-grey-400">
                <p className="block ml-2 font-bold">
                  {props.modalData.external_data.name}
                </p>
              </header>
              <div>
                <p className="text-sm	text-gray-500">Owner</p>
              </div>
              <div>
                <p className="text-ownerSize">{props.modalData.owner}</p>
              </div>
              <div>
                <p className="text-sm	text-gray-500 mt-8">Description</p>
              </div>
              <div>
                <h2>
                  {props.modalData.description
                    ? props.modalData.description
                    : "No data"}
                </h2>
              </div>
              <div>
                <h2
                  className="mt-12 text-center"
                  onClick={() => {
                    window.open(props.modalData.external_data.external_url);
                  }}
                >
                  Link
                </h2>
              </div>
              <button
                type="button"
                className="inline-flex mt-24 items-center px-4 py-2 border border-purple-400 text-base leading-6 font-medium rounded-md text-purple-800 bg-white hover:text-purple-700 focus:border-purple-300 transition ease-in-out duration-150"
                onClick={() => props.setOpenModal(false)}
              >
                Back
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Popup;
