import './slide.scss';

function Slide({nft}) {
  return (
    <div>
      <div className="min-w-screen min-h-screen flex items-center justify-center overflow-hidden">

      {nft.external_data.attributes[3].value === "INVOKED" ? <img width="300vw" height="300vw" alt="character" src={"https://cryptoseals.art/characters/" + nft.external_data.attributes[6].value + "/" + nft.external_data.attributes[5].value + ".gif"} /> : <div></div> }
      </div>
    </div>
  );
}

export default Slide;