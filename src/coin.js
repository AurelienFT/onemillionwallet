import { useEffect, useState } from "react";
import {
  LineChart,
  XAxis,
  YAxis,
  Tooltip,
  Line,
  Legend,
  ResponsiveContainer,
} from "recharts";

import Mainnets from "./mainnets.json";
import Currencies from "./currencies.json";
import { useHistory } from "react-router-dom";

import {
  getTokenBySymbol,
  getValueOverTimeByToken,
  getTransactionsBySymbol,
} from "./covalent";

function CreateTr(props) {
  const [value, setValue] = useState();

  function unavailableData() {
    if (props.value === null || props.value === undefined)
      setValue("Unavailable data");
    else setValue(props.value);
  }

  useEffect(() => {
    unavailableData();
  });

  return (
    <tr
      className="border-b border-gray-200 hover:bg-gray-100 cursor-pointer"
      key={props.index}
      onClick={() =>
        window.open("https://cchain.explorer.avax.network/tx/" + props.id)
      }
    >
      <td className="py-3 px-6 text-left whitespace-nowrap">
        {props.type === "IN" ? (
          <div className="flex">
            <p className="text-green-500">IN</p>
          </div>
        ) : (
          <div className="flex">
            <p className="text-red-500">OUT</p>
          </div>
        )}
      </td>
      <td className="py-3 px-6 text-left whitespace-nowrap">
        {props.date === null ? (
          <div className="flex">
            <span className="dark:text-white">No data</span>
          </div>
        ) : (
          <div className="flex">
            <span className="dark:text-white">{props.date}</span>
          </div>
        )}
      </td>
      <td className="py-3 px-6 text-left">
        {props.recever === null ? (
          <div className="flex">
            <span className="dark:text-white">No data</span>
          </div>
        ) : (
          <div className="flex">
            <span className="dark:text-white">{props.contract_name ? props.recever : props.recever}</span>
          </div>
        )}
        {/* <div className="flex">
                    <span>{recever}</span>
                </div> */}
      </td>
      <td className="py-3 px-6 text-left">
        <div className="flex float-right mr-12">
          <span className="dark:text-white">{props.type === "IN" ? "+ " + value : "- " + value}</span>
        </div>
      </td>
      <td className="py-3 px-6 text-left whitespace-nowrap">
        {props.fees === null ? (
          <div className="flex">
            <span className="dark:text-white">No data</span>
          </div>
        ) : (
          <div className="flex">
            <span className="dark:text-white">{props.fees} AVAX</span>
          </div>
        )}
      </td>
    </tr>
  );
}

function Coin(props) {
  const [darkMode, setDarkMode] = useState("no");
  const [balance, setBalance] = useState("1");
  const [number, setNumber] = useState("1");
  const [currencySymbol, setCurrencySymbol] = useState("$");
  const [chartData, setChartData] = useState(null);
  const [cryptoArray, setCryptoArray] = useState(null);
  const [logoUri, setLogoUri]  = useState("");
  const [projectName, setProjectName] = useState("");
  // 
  const history = useHistory()

  useEffect(() => {
    async function createCryptoGraph(currency) {
      let mainnet = Mainnets.find(
        (mainnetObject) => mainnetObject.name === props.match.params.mainnet
      ).covalent_id;
      const res = await getTokenBySymbol(
        mainnet,
        props.match.params.address,
        currency,
        props.match.params.symbol
      );
      if (res.contract_name === "Avalanche Coin") {
        setLogoUri("https://avascan.info/cdn/images/assets/avax.svg");
      } else {
        setLogoUri(process.env.PUBLIC_URL + "/crypto_logos/" + res.contract_address + "/logo.png")
      }
      setProjectName(res.contract_name)
      setBalance(res.quote);
      setNumber(res.balance / Math.pow(10, res.contract_decimals));
      const res_graph = await getValueOverTimeByToken(
        mainnet,
        props.match.params.address,
        currency,
        props.match.params.symbol
      );
      setChartData(res_graph);
    }

    async function createTransactionArray(currency) {
      let mainnet = Mainnets.find(
        (mainnetObject) => mainnetObject.name === props.match.params.mainnet
      ).covalent_id;
      const res = await getTransactionsBySymbol(
        mainnet,
        props.match.params.address,
        currency,
        props.match.params.symbol
      );
      setCryptoArray(res);
    }

    let currency = localStorage.getItem("currency") || "usd";
    let currencies = Currencies.find(
      (currenciesObject) => currenciesObject.value === currency
    ).symbol;
    setCurrencySymbol(currencies);
    createCryptoGraph(currency);
    createTransactionArray(currency);
    var darkmode = localStorage.getItem("darkmode");
    if (darkmode) {
      setDarkMode(darkmode);
    } else if (!darkmode) {
      localStorage.setItem("darkmode", "no");
      setDarkMode("no");
    }
  }, [props]);

  function prevPage () {
    history.push("/")
  }

  return (
    <div className={darkMode === "yes" ? "dark" : ""}>
      <div className="min-h-screen bg-auto bg-no-repeat bg-center bg-blue-400 dark:bg-gray-800">
      <button class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l" onClick={() => prevPage()}>
        Home
      </button>
      <div className="-mb-20 flex bg-auto items-center justify-center overflow-hidden flex dark:text-white">
          <div className="w-36 h-36 flex flex-wrap content-center dark:text-white">
            <img className="rounded-xl" width="100" heught="100" src={logoUri} alt="logo_crypto"></img>
            </div>
          <div className="text-4xl font-medium flex item-center">{projectName}</div>
        </div>
        <div className="min-w-screen min-h-screen flex items-center justify-center overflow-hidden flex flex-col">
          <div className="container w-full lg:w-5/6 rounded-lg overflow-hidden shadow-lg bg-white dark:bg-gray-600">
            {chartData ? (
              <div>
                <div className="mb-8 min-w-screen rounded flex items-center justify-center">
                  <span>
                    <div className="antialiased text-gray-300 flex items-center justify-center">
                      Balance
                    </div>
                    <div className="text-4xl font-medium text-black flex items-center justify-center dark:text-white">
                      {number}
                    </div>
                    <div className="text-xl font-medium text-black flex items-center justify-center  dark:text-gray-400">
                      {balance} {currencySymbol}
                    </div>
                  </span>
                </div>
                <div className="min-w-screen flex items-center justify-center h-96">
                  <ResponsiveContainer width="100%" height="100%">
                    <LineChart
                      width={500}
                      height={300}
                      data={chartData}
                      margin={{
                        top: 5,
                        right: 30,
                        left: 20,
                        bottom: 5,
                      }}
                    >
                      {/* <CartesianGrid strokeDasharray="3 3" /> */}
                      <XAxis dataKey="time" stroke={darkMode === "yes" ? "#ffffff": "#787878"} />
                      <YAxis yAxisId="left" stroke="#8884d8" />
                      <YAxis
                        yAxisId="right"
                        stroke="#82ca9d"
                        orientation="right"
                      />
                      <Tooltip />
                      <Legend />
                      <Line
                        yAxisId="left"
                        type="monotone"
                        strokeWidth={3}
                        dataKey="value"
                        stroke="#8884d8"
                        activeDot={{ r: 8 }}
                      />
                      <Line
                        yAxisId="right"
                        type="monotone"
                        strokeWidth={3}
                        dataKey="balance"
                        stroke="#82ca9d"
                      />
                    </LineChart>
                  </ResponsiveContainer>
                </div>
              </div>
            ) : (
              <div className="min-h-screen bg-auto bg-no-repeat bg-center bg-gradient-to-b from-blue-500 to-blue-100 ...">
                <div className="top-0 right-0 h-screen flex justify-center items-center">
                  <div className="animate-spin rounded-full h-16 w-16 border-t-2 border-b-2 border-gray-900"></div>
                </div>
              </div>
            )}
          </div>
          <div className="w-full lg:w-5/6">
            {cryptoArray ? (
              <div className="bg-white shadow-md rounded my-6 dark:bg-gray-600">
                <table className="min-w-max w-full table-auto">
                  <thead>
                    <tr
                      className="text-gray-400 text-sm leading-normal dark:text-white"
                      key="header"
                    >
                      <th className="py-3 px-6 text-left"></th>
                      <th className="py-3 px-6 text-left">Date UTC</th>
                      <th className="py-3 px-6 text-left">From/to</th>
                      <th className="py-3 px-6 text-left">
                        Total Value
                      </th>
                      <th className="py-3 px-6 text-left">
                        Fees
                      </th>
                    </tr>
                  </thead>

                  <tbody className="text-gray-600 text-sm font-light">
                    {cryptoArray.map((item, index) => {
                      return (
                        <CreateTr
                          date={item.date}
                          id={item.tx_id}
                          value={item.amount}
                          recever={item.sender}
                          contract_name={item.contract_name}
                          fees={item.fees}
                          type={item.type}
                          darkMode={darkMode}
                          index={index}
                          key={index}
                        ></CreateTr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            ) : (
              <div className="top-0 right-0 h-screen flex justify-center items-center">
                <div className="animate-spin rounded-full h-16 w-16 border-t-2 border-b-2 border-gray-900"></div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Coin;
