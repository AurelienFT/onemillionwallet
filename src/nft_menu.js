import { useEffect, useState } from "react";
import { getNFTs } from "./covalent";
import "./nft_menu.css";
import Mainnets from "./mainnets.json";
import { useHistory } from "react-router-dom";

function NFT_Menu(props) {
  const [nftData, setNftData] = useState(null);
  let history = useHistory();

  function handleClick(symbol) {
    history.push("/nft/" + props.mainnetName + "/" + props.address + "/" + symbol);
  }

  useEffect(() => {
    async function getNftData() {
      let mainnet = Mainnets.find(
        (mainnetObject) => mainnetObject.name === props.mainnetName
      ).covalent_id;
      setNftData(await getNFTs(mainnet, props.address));
    }
    if (!props.reload) {
      return;
    }
    getNftData();
  }, [props]);
  return (
    <div>
      <div className="flex items-center justify-center">
        <div className="text-2xl w-full lg:w-1/6 text-center dark:text-white">NFTs</div>
      </div>
      {nftData ? (
        <div className="flex flex-wrap justify-center items-center gap-3 py-5">
          {nftData.map((item, index) => {
            if (!item.nft_data) {
              return (<div></div>)
            }
            return (
              <div className="container mx-auto max-w-xs rounded-lg overflow-hidden shadow-lg my-2 bg-white dark:bg-gray-600" key={index}>
                <div className="relative mb-6">
                  <img
                    className="w-full"
                    src={item.image_menu}
                    alt="Profile"
                  />
                  <div
                    className="text-center absolute w-full"
                    style={{ bottom: "-30px" }}
                  >
                    <div className="mb-10">
                      <p className="text-white tracking-wide uppercase text-lg font-bold">
                        {item.contract_name}
                      </p>
                      <a className="text-gray-400 text-sm" href={item.website} rel="noreferrer" target="_blank">{item.website}</a>
                    </div>
                    <button className="p-4 rounded-full transition ease-in duration-200 focus:outline-none button_nft" onClick={() => {handleClick(item.contract_ticker_symbol)}}>
                      <svg
                        viewBox="0 0 20 20"
                        enableBackground="new 0 0 20 20"
                        className="w-6 h-6"
                      >
                        <path
                          fill="#FFFFFF"
                          d="M16,10c0,0.553-0.048,1-0.601,1H11v4.399C11,15.951,10.553,16,10,16c-0.553,0-1-0.049-1-0.601V11H4.601
                     C4.049,11,4,10.553,4,10c0-0.553,0.049-1,0.601-1H9V4.601C9,4.048,9.447,4,10,4c0.553,0,1,0.048,1,0.601V9h4.399
                     C15.952,9,16,9.447,16,10z"
                        />
                      </svg>
                    </button>
                  </div>
                </div>
                <div className="py-10 px-6 text-center tracking-wide grid grid-cols-2 gap-6">
                  <div className="posts">
                    <p className="text-lg dark:text-white">{item.nft_data.length}</p>
                    <p className="text-gray-400 text-sm">NFT</p>
                  </div>
                  <div className="posts">
                    <p className="text-lg dark:text-white">{item.contract_ticker_symbol}</p>
                    <p className="text-gray-400 text-sm">Symbol</p>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      ) : (
        <div className="top-0 right-0 h-screen flex justify-center items-center">
          <div className="animate-spin rounded-full h-16 w-16 border-t-2 border-b-2 border-gray-900"></div>
        </div>
      )}
    </div>
  );
}

export default NFT_Menu;
