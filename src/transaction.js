import { useEffect, useState } from "react";
import { getPortfolio, getGlobalValueOverTime } from "./covalent";
import {
  AreaChart,
  Area,
  XAxis,
  YAxis,
  Tooltip,
  ResponsiveContainer,
} from "recharts";
import Mainnets from "./mainnets.json";
import Currencies from "./currencies.json";
import "./loader.css";
import { useHistory } from "react-router-dom";

function CreateTr(props) {
  const [isImgError, setIsImgError] = useState(false);
  const [price, setPrice] = useState("");
  const [project, setProject] = useState("");
  const [pocessedNumber, setPocessedNumber] = useState("");
  const [percentage, setPercentage] = useState("");
  const history = useHistory();

  function unAvailableData() {
    if (props.project !== null && props.project !== undefined)
      setProject(props.project);
    else {
      setProject("Unavailable data");
    }
    if (props.price !== null && props.price !== undefined)
      setPrice(props.price + " " + props.symbol);
    else {
      setPrice("Unavailable data");
    }
    if (props.pocessedNumber !== null && props.pocessedNumber !== undefined)
      setPocessedNumber(props.pocessedNumber);
    else {
      setPocessedNumber("Unavailable data");
    }
    if (props.percentage !== null && props.percentage !== undefined)
      setPercentage(props.percentage);
    else {
      setPercentage("Unavailable data");
    }
  }

  useEffect(() => {
    unAvailableData();
  });

  function default_img() {
    setIsImgError(true);
  }

  function percentageColor() {
    if (percentage > 0)
      return (
        <span className="bg-green-200 text-white-600 py-1 px-3 rounded-full text-xs">
          {parseFloat(percentage).toFixed(2)} %
        </span>
      );
    else
      return (
        <span className="bg-red-200 text-white-600 py-1 px-3 rounded-full text-xs">
          {parseFloat(percentage).toFixed(2)} %
        </span>
      );
  }

  function openCoin(symbol) {
    history.push(
      "/coin/" + props.mainnetName + "/" + symbol + "/" + props.address
    );
  }

  return (
    <tr
      className="border-b border-gray-200 hover:bg-gray-100 cursor-pointer"
      key={props.index}
      onClick={() => openCoin(props.symbolCrypto)}
    >
      <td className="py-3 px-6 text-left whitespace-nowrap">
        <div className="flex items-center">
          <div className="mr-2">
            {isImgError ? (
              <img
                width="24"
                height="24"
                src={process.env.PUBLIC_URL + "/crypto_logos/basic.png"}
                alt=""
              ></img>
            ) : (
              <img
                width="24"
                height="24"
                src={props.logoUri}
                onError={() => {
                  default_img();
                }}
                alt=""
              ></img>
            )}
          </div>
          <span className="uppercase dark:text-white">{project}</span>
        </div>
      </td>
      <td className="py-3 px-6 text-left">
        <div className="flex items-center">
          <div className="mr-2">
            <table>
              <tbody>
                <tr key={"pocessed"}>
                  <td>
                    <span className="dark:text-white">
                      {parseFloat(pocessedNumber).toFixed(5)}
                    </span>
                  </td>
                </tr>
                <tr key={"value"}>
                  <td>
                    <span className="text-gray-400 dark:text-white">
                      {(parseFloat(price) * parseFloat(pocessedNumber)).toFixed(
                        2
                      )}{" "}
                      {props.symbol}
                    </span>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </td>
      <td className="py-3 px-6 text-left">
        <div className="flex items-center justify-center">
          <span className="dark:text-white">{price}</span>
        </div>
      </td>
      <td className="py-3 px-6 text-left">
        <div className="flex items-center justify-center">
          {percentageColor()}
        </div>
      </td>
    </tr>
  );
}

function Transaction(props) {
  const [cryptoArray, setCryptoArray] = useState(null);
  const [chartData, setChartData] = useState(null);
  const [symbol, setSymbol] = useState("");

  useEffect(() => {
    async function createCryptoGraph() {
      let mainnet = Mainnets.find(
        (mainnetObject) => mainnetObject.name === props.mainnetName
      ).covalent_id;

      const res = await getGlobalValueOverTime(
        mainnet,
        props.address,
        props.currency
      );
      setChartData(res);
    }

    async function createCryptoArray() {
      let mainnet = Mainnets.find(
        (mainnetObject) => mainnetObject.name === props.mainnetName
      ).covalent_id;

      setCryptoArray(
        await getPortfolio(mainnet, props.address, props.currency)
      );
    }
    if (!props.reload) return;
    setCryptoArray(null);
    setChartData(null);
    let currencies = Currencies.find(
      (currenciesObject) => currenciesObject.value === props.currency
    ).symbol;
    setSymbol(currencies);
    createCryptoArray();
    createCryptoGraph();
  }, [props]);

  return (
    <div className="min-w-screen min-h-screen flex items-center justify-center overflow-hidden flex flex-col">
      <div className="container w-full lg:w-5/6 rounded-lg overflow-hidden shadow-lg bg-white dark:bg-gray-600">
        {chartData ? (
          <div>
            <div className="mb-8 min-w-screen rounded flex items-center justify-center">
              <span>
                <div className="antialiased text-gray-300 flex items-center justify-center">
                  Balance
                </div>
                <div className="text-4xl font-medium text-black flex items-center justify-center dark:text-white">
                  {chartData.length === 0 || chartData === undefined
                    ? ""
                    : chartData[chartData.length - 1].value + " " + symbol}
                </div>
              </span>
            </div>
            <div className="min-w-screen flex items-center justify-center h-96">
              <ResponsiveContainer
                width="100%"
                height={400}
                text="test"
                className="min-w-screen min-h-screen flex items-center flex items-center justify-center h-96"
              >
                <AreaChart
                  width={220}
                  height={200}
                  data={chartData}
                  margin={{ top: 0, right: 5, bottom: 3, left: 0 }}
                >
                  <XAxis
                    dataKey="time"
                    stroke={props.darkMode === "yes" ? "#ffffff" : "#787878"}
                  />
                  <YAxis
                    stroke={props.darkMode === "yes" ? "#ffffff" : "#787878"}
                  />
                  <Tooltip />
                  <Area
                    type="monotone"
                    dataKey="value"
                    stroke="#8884d8"
                    fill="#8884d8"
                  />
                  <Area datakey="value"></Area>
                </AreaChart>
              </ResponsiveContainer>
            </div>
          </div>
        ) : (
          <div className="top-0 right-0 h-screen flex justify-center items-center">
            <div className="animate-spin rounded-full h-16 w-16 border-t-2 border-b-2 border-gray-900"></div>
          </div>
        )}
      </div>
      <div className="flex items-center justify-center">
        <div className="text-2xl w-full lg:w-1/6 text-center dark:text-white">
          Coins
        </div>
      </div>
      <div className="w-full lg:w-5/6">
        {cryptoArray ? (
          <div className="bg-white shadow-md rounded my-6 dark:bg-gray-600">
            <table className="min-w-max w-full table-auto">
              <thead>
                <tr
                  className="text-gray-400 text-sm leading-normal dark:text-white"
                  key="header"
                >
                  <th className="py-3 px-6 text-left">Project</th>
                  <th className="py-3 px-6 text-left">Balance</th>
                  <th className="py-3 px-6 text-center">Price</th>
                  <th className="py-3 px-6 text-center">24h change%</th>
                </tr>
              </thead>

              <tbody className="text-gray-600 text-sm font-light">
                {cryptoArray.map((item, index) => {
                  return (
                    <CreateTr
                      logoUri={item.logoUri}
                      project={item.project}
                      price={item.price}
                      symbolCrypto={item.symbol}
                      mainnetName={props.mainnetName}
                      address={props.address}
                      pocessedNumber={item.pocessedNumber}
                      percentage={item.percentage}
                      index={index}
                      symbol={symbol}
                      darkMode={props.darkMode}
                      key={index}
                    ></CreateTr>
                  );
                })}
              </tbody>
            </table>
          </div>
        ) : (
          <div className="top-0 right-0 h-screen flex justify-center items-center">
            <div className="animate-spin rounded-full h-16 w-16 border-t-2 border-b-2 border-gray-900"></div>
          </div>
        )}
      </div>
    </div>
  );
}

export default Transaction;
