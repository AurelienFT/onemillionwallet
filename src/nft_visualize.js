import "./nft_visualize.scss";
import React, { useState, useEffect } from "react";
import { getNFTs } from "./covalent";
import Mainnets from "./mainnets.json";
import { useHistory } from "react-router-dom";

import DefaultSlide from "./NFT/Default/Slide";
import AvaxtarsSlide from "./NFT/Avaxtars/Slide";
import CryptoSealsSlide from "./NFT/CryptoSeals/Slide";

import DefaultPopup from "./NFT/Default/Popup";
import AvaxtarsPopup from "./NFT/Avaxtars/Popup";
import CryptoSealsPopup from "./NFT/CryptoSeals/Popup";

function useTilt(active) {
  const ref = React.useRef(null);

  React.useEffect(() => {
    if (!ref.current || !active) {
      return;
    }

    const state = {
      rect: undefined,
      mouseX: undefined,
      mouseY: undefined,
    };

    let el = ref.current;

    const handleMouseMove = (e) => {
      if (!el) {
        return;
      }
      if (!state.rect) {
        state.rect = el.getBoundingClientRect();
      }
      state.mouseX = e.clientX;
      state.mouseY = e.clientY;
      const px = (state.mouseX - state.rect.left) / state.rect.width;
      const py = (state.mouseY - state.rect.top) / state.rect.height;

      el.style.setProperty("--px", px);
      el.style.setProperty("--py", py);
    };

    el.addEventListener("mousemove", handleMouseMove);

    return () => {
      el.removeEventListener("mousemove", handleMouseMove);
    };
  }, [active]);

  return ref;
}

const initialState = {
  slideIndex: 0,
};

function Slide({ setOpenModal, slide, offset, index, resultName }) {
  const active = offset === 0 ? true : null;
  const ref = useTilt(active);

  function slideContent(resultName) {
    switch(resultName) {
      case "Avaxtars":
        return (
          <AvaxtarsSlide nft={slide}></AvaxtarsSlide>
        )
      case "CryptoSeals":
        return (
          <CryptoSealsSlide nft={slide}></CryptoSealsSlide>
        )
      default:
        return (
          <DefaultSlide nft={slide}></DefaultSlide>
        )
    }
  }

  return (
    <div
      ref={ref}
      className="slide"
      data-active={active}
      style={{
        "--offset": offset,
        "--dir": offset === 0 ? 0 : offset > 0 ? 1 : -1,
      }}
    >
      <div
        className="slideContent"
        style={{
          backgroundImage: `url('${slide.external_data.image}')`,
        }}
        onClick={() => {
          setOpenModal(index);
        }}
      >
        <div className="slideContentInner">
          {
            slideContent(resultName)
          }
        </div>
      </div>
    </div>
  );
}

function NftVisualize(props) {
  const [openModal, setOpenModal] = useState(false);
  const [slides, setSlides] = useState([]);
  const [resultName, setResultName] = useState([]);
  const [dataModal, setDataModal] = useState(null);

  const slidesReducer = (state, event) => {
    if (event.type === "PREV") {
      return {
        ...state,
        slideIndex: (state.slideIndex + 1) % slides.length,
      };
    }
    if (event.type === "NEXT") {
      return {
        ...state,
        slideIndex:
          state.slideIndex === 0 ? slides.length - 1 : state.slideIndex - 1,
      };
    }
  };

  const [state, dispatch] = React.useReducer(slidesReducer, initialState);
  let history = useHistory();

  useEffect(() => {
    async function getNftData() {
      let mainnet = Mainnets.find(
        (mainnetObject) => mainnetObject.name === props.match.params.mainnet
      ).covalent_id;
      let tokens = await getNFTs(mainnet, props.match.params.address);
      if (tokens.length === 0 || !tokens) {
        return;
      }
      var collection = tokens.find(
        (tokenObject) =>
          tokenObject.contract_ticker_symbol === props.match.params.symbol
      );
      if (!collection || !collection.nft_data) {
        return;
      }
      setResultName(collection.contract_name);
      setSlides(collection.nft_data);
    }
    getNftData();
  }, [props]);

  function handleChange(index) {
    if (openModal === false) {
      setDataModal(slides[index]);
    } else {
      setDataModal(null);
    }
    setOpenModal(!openModal);
  }

  function PopupContent(modalData) {
    switch(resultName) {
      case "Avaxtars":
        return (
          <AvaxtarsPopup modalData={modalData} setOpenModal={setOpenModal}></AvaxtarsPopup>
        )
      case "CryptoSeals":
        return (
          <CryptoSealsPopup modalData={modalData} setOpenModal={setOpenModal}></CryptoSealsPopup>
        )
      default:
        return (
          <DefaultPopup modalData={modalData} setOpenModal={setOpenModal}></DefaultPopup>
        )
    }
  }

  return (
    <div className="bg-black">
      <div className="text-white">
      <button className="border border-teal-500 bg-teal-500 text-white block rounded-sm font-bold py-4 px-6 ml-2 flex items-center" onClick={() => {    history.push("/")}}>
      <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" className="bi bi-arrow-left" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
</svg>
        Back
    </button>
      </div>
      <div className="classBody">
        {slides.length !== 0 ? (
          <div className="slides">
            <button onClick={() => dispatch({ type: "PREV" })}>‹</button>

            {[...slides, ...slides, ...slides].map((slide, i) => {
              let offset = slides.length + (state.slideIndex - i);

              return (
                <Slide
                  setOpenModal={handleChange}
                  slide={slide}
                  offset={offset}
                  index={i % slides.length}
                  resultName={resultName}
                  key={i}
                />
              );
            })}
            <button onClick={() => dispatch({ type: "NEXT" })}>›</button>
          </div>
        ) : (
          <div className="top-0 right-0 h-screen flex justify-center items-center">
            <div className="animate-spin border-red-600 rounded-full h-16 w-16 border-t-2 border-b-2 border-gray-900"></div>
          </div>
        )}
        {openModal ? PopupContent(dataModal) : (
          <div></div>
        )}
      </div>
    </div>
  );
}

export default NftVisualize;
