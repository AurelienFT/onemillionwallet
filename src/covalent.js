const { default: axios } = require("axios");
const id_coingecko = require("./id_coingecko.json");
const nft_objects = require("./nft_supported.json");

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

async function getPortfolio(mainnet_covalent_id, address, currency) {
  let portfolio = (
    await axios.get(
      "https://api.covalenthq.com/v1/" +
        mainnet_covalent_id +
        "/address/" +
        address +
        "/balances_v2/?key=" +
        process.env.REACT_APP_COVALENT_API_KEY +
        "&nft=false&quote-currency=" +
        currency
    )
  ).data;
  if (portfolio.error) {
    return null;
  }
  let data = portfolio.data;
  // Remove dust
  data.items = data.items.filter(
    (item) => !(item.type === "dust" || item.type === "nft")
  );
  var returned_array = [];
  for (const item of data.items) {
    if (item.contract_ticker_symbol === "PGL") {
      continue;
    }
    var element = {
      project: null,
      symbol: null,
      logoUri: null,
      price: null,
      pocessedNumber: null,
      percentage: null,
    };
    element.project = item.contract_name;
    element.symbol = item.contract_ticker_symbol;
    element.pocessedNumber =
      item.balance / Math.pow(10, item.contract_decimals);
    if (item.quote_rate === null) {
      var crypto_on_gecko = null;
      if (item.contract_ticker_symbol === "SPORE") {
        crypto_on_gecko = { id: "spore-finance-2" };
      } else if (item.contract_ticker_symbol === "ANY") {
        crypto_on_gecko = { id: "anyswap" };
      } else if (item.contract_ticker_symbol === "MUSH") {
        crypto_on_gecko = null
      } else {
        crypto_on_gecko = id_coingecko.find(
          (object) =>
            object.symbol === item.contract_ticker_symbol.toLowerCase() ||
            object.symbol === item.contract_ticker_symbol ||
            object.symbol === item.contract_ticker_symbol.toUpperCase()
        );
      }
      if (!crypto_on_gecko) {
        returned_array.push(element);
        continue;
      };
      let resp = await axios.get(
        "https://api.coingecko.com/api/v3/coins/" + crypto_on_gecko.id
      );
      if (resp.data) {
        element.price =
          resp.data.market_data.current_price[currency.toLowerCase()];
      }
    } else {
      element.price = item.quote_rate;
    }
    if (element.price != null) {
      var crypto_on_gecko_2 = null;
      if (item.contract_ticker_symbol === "SPORE") {
        crypto_on_gecko_2 = { id: "spore-finance-2" };
      } else if (item.contract_ticker_symbol === "ANY") {
        crypto_on_gecko_2 = { id: "anyswap" };
      } else {
        crypto_on_gecko_2 = id_coingecko.find(
          (object) =>
            object.symbol === item.contract_ticker_symbol.toLowerCase() ||
            object.symbol === item.contract_ticker_symbol ||
            object.symbol === item.contract_ticker_symbol.toUpperCase()
        );
      }
      if (!crypto_on_gecko_2) continue;
      let actual_date = new Date().getTime() / 1000;
      let yesterday_date = new Date();
      yesterday_date.setDate(new Date().getDate() - 1);
      yesterday_date = yesterday_date.getTime() / 1000;
      let resp_charts = await axios.get(
        "https://api.coingecko.com/api/v3/coins/" +
          crypto_on_gecko_2.id +
          "/market_chart/range?vs_currency=" +
          currency.toLowerCase() +
          "&from=" +
          yesterday_date +
          "&to=" +
          actual_date
      );
      if (
        resp_charts.data &&
        resp_charts.data.prices &&
        resp_charts.data.prices.length > 0
      ) {
        element.percentage =
          ((element.price - resp_charts.data.prices[0][1]) /
            resp_charts.data.prices[0][1]) *
          100;
      }
    }

    if (mainnet_covalent_id === 43114 || mainnet_covalent_id === 43113) {
      if (item.contract_name === "Avalanche Coin") {
        element.logoUri = "https://avascan.info/cdn/images/assets/avax.svg";
      } else {
        element.logoUri =
          process.env.PUBLIC_URL +
          "/crypto_logos/" +
          item.contract_address +
          "/logo.png";
      }
    } else {
      element.logoUri = item.logo_url;
    }
    returned_array.push(element);
    await sleep(300);
  }
  return returned_array;
}

async function getGlobalValueOverTime(mainnet_covalent_id, address, currency) {
  let value_over_time = (
    await axios.get(
      "https://api.covalenthq.com/v1/" +
        mainnet_covalent_id +
        "/address/" +
        address +
        "/portfolio_v2/?key=" +
        process.env.REACT_APP_COVALENT_API_KEY +
        "&quote-currency=" +
        currency
    )
  ).data;
  let data_returned = [];
  for (const item of value_over_time.items) {
    if (!item.holdings || item.holdings.length === 0) {
      continue;
    }
    if (item.contract_ticker_symbol === "PGL" || item.contract_ticker_symbol === "MUSH") {
      continue;
    }
    if (item.holdings[0].close.balance === "0") {
      continue;
    }
    let coin_gecko_data = [];
    let index_zero = item.holdings.findIndex(
      (holding) => holding.quote_rate === null || holding.quote_rate === 0
    );
    if (index_zero !== -1) {
      var crypto_on_gecko = null;
      if (item.contract_ticker_symbol === "SPORE") {
        crypto_on_gecko = { id: "spore-finance-2" };
      } else if (item.contract_ticker_symbol === "ANY") {
        crypto_on_gecko = { id: "anyswap" };
      } else {
        crypto_on_gecko = id_coingecko.find(
          (object) =>
            object.symbol === item.contract_ticker_symbol.toLowerCase() ||
            object.symbol === item.contract_ticker_symbol ||
            object.symbol === item.contract_ticker_symbol.toUpperCase()
        );
      }
      if (!crypto_on_gecko) {
        continue;
      }
      await sleep(200);
      let startDate = new Date(item.holdings[0].timestamp);
      let endDate = new Date(item.holdings[30].timestamp);
      let historical_data = (
        await axios.get(
          "https://api.coingecko.com/api/v3/coins/" +
            crypto_on_gecko.id +
            "/market_chart/range?vs_currency=" +
            currency +
            "&from=" +
            endDate.getTime() / 1000 +
            "&to=" +
            startDate.getTime() / 1000
        )
      ).data.prices;
      if (!historical_data || historical_data.length === 0) {
        continue;
      }
      coin_gecko_data = historical_data;
    }
    for (const holding of item.holdings) {
      var quote_rate = 0;
      if (holding.quote_rate === null || holding.quote_rate === 0) {
        let actual_date = new Date(holding.timestamp).getTime();
        coin_gecko_data.sort(function (a, b) {
          var distancea = Math.abs(actual_date - a[0]);
          var distanceb = Math.abs(actual_date - b[0]);
          return distancea - distanceb;
        });
        quote_rate = coin_gecko_data[0][1];
      } else {
        quote_rate = holding.quote_rate;
      }
      if (quote_rate === 0) {
        continue;
      }
      let value_usd =
        (holding.close.balance / Math.pow(10, item.contract_decimals)) *
        quote_rate;
      let date = new Date(holding.timestamp);
      let formatted_date =
        date.getFullYear() +
        "-" +
        ("0" + (date.getMonth() + 1)).slice(-2) +
        "-" +
        ("0" + date.getDate()).slice(-2);
      let index = data_returned.findIndex(
        (historical_object) => historical_object.time === formatted_date
      );
      if (index === -1) {
        data_returned.push({
          time: formatted_date,
          value: parseFloat(value_usd).toFixed(2),
        });
      } else {
        data_returned[index].value = (
          parseFloat(data_returned[index].value) + parseFloat(value_usd)
        ).toFixed(2);
      }
    }
  }
  return data_returned.reverse();
}

async function getNFTs(mainnet_covalent_id, address) {
  let portfolio = (
    await axios.get(
      "https://api.covalenthq.com/v1/" +
        mainnet_covalent_id +
        "/address/" +
        address +
        "/balances_v2/?key=" +
        process.env.REACT_APP_COVALENT_API_KEY +
        "&nft=true"
    )
  ).data;
  if (portfolio.error) {
    return null;
  }
  let data = portfolio.data;
  // Remove dust and cryptocurrencies
  data.items = data.items.filter(
    (item) => !(item.type === "dust" || item.type === "cryptocurrency")
  );
  data.items = data.items.map((item) => {
    let nft_object = nft_objects.find((object) => {
      return object.name === item.contract_name;
    });
    if (nft_object) {
      item.image_menu =
        process.env.PUBLIC_URL + "/nft/" + nft_object.image_menu;
      item.website = nft_object.website;
    } else {
      item.image_menu = process.env.PUBLIC_URL + "/nft/default.png";
      item.website = "";
    }
    return item;
  });
  return data.items;
}

async function getTokenBySymbol(
  mainnet_covalent_id,
  address,
  currency,
  symbol
) {
  let portfolio = (
    await axios.get(
      "https://api.covalenthq.com/v1/" +
        mainnet_covalent_id +
        "/address/" +
        address +
        "/balances_v2/?key=" +
        process.env.REACT_APP_COVALENT_API_KEY +
        "&nft=false&quote-currency=" +
        currency
    )
  ).data;
  if (portfolio.error) {
    return null;
  }
  var item = portfolio.data.items.find((tokenObject) => {
    return tokenObject.contract_ticker_symbol === symbol;
  });
  if (item && item.quote_rate === null) {
    var crypto_on_gecko = null;
    if (item.contract_ticker_symbol === "SPORE") {
      crypto_on_gecko = { id: "spore-finance-2" };
    } else if (item.contract_ticker_symbol === "ANY") {
      crypto_on_gecko = { id: "anyswap" };
    } else {
      crypto_on_gecko = id_coingecko.find(
        (object) =>
          object.symbol === item.contract_ticker_symbol.toLowerCase() ||
          object.symbol === item.contract_ticker_symbol ||
          object.symbol === item.contract_ticker_symbol.toUpperCase()
      );
    }
    if (!crypto_on_gecko) {
      return item;
    }
    let resp = await axios.get(
      "https://api.coingecko.com/api/v3/coins/" + crypto_on_gecko.id
    );
    if (resp.data) {
      item.quote = (
        parseFloat(
          resp.data.market_data.current_price[currency.toLowerCase()]
        ) * parseFloat(item.balance / Math.pow(10, item.contract_decimals))
      ).toFixed(2);
    }
  }
  return item;
}

async function getValueOverTimeByToken(
  mainnet_covalent_id,
  address,
  currency,
  symbol
) {
  let value_over_time = (
    await axios.get(
      "https://api.covalenthq.com/v1/" +
        mainnet_covalent_id +
        "/address/" +
        address +
        "/portfolio_v2/?key=" +
        process.env.REACT_APP_COVALENT_API_KEY +
        "&quote-currency=" +
        currency
    )
  ).data;
  let item = value_over_time.items.find((tokenObject) => {
    return tokenObject.contract_ticker_symbol === symbol;
  });
  if (!item) {
    return [];
  }
  let data_returned = [];
  if (item.contract_ticker_symbol === "PGL") {
    return [];
  }
  if (item.holdings[0].close.balance === "0") {
    return [];
  }
  let coin_gecko_data = [];
  let index_zero = item.holdings.findIndex(
    (holding) => holding.quote_rate === null || holding.quote_rate === 0
  );
  var crypto_on_gecko = null;
  if (index_zero !== -1) {
    if (item.contract_ticker_symbol === "SPORE") {
      crypto_on_gecko = { id: "spore-finance-2" };
    } else if (item.contract_ticker_symbol === "ANY") {
      crypto_on_gecko = { id: "anyswap" };
    } else {
      crypto_on_gecko = id_coingecko.find(
        (object) =>
          object.symbol === item.contract_ticker_symbol.toLowerCase() ||
          object.symbol === item.contract_ticker_symbol ||
          object.symbol === item.contract_ticker_symbol.toUpperCase()
      );
    }
    if (!crypto_on_gecko) {
      return [];
    }
    let startDate = new Date(item.holdings[0].timestamp);
    let endDate = new Date(item.holdings[30].timestamp);
    let historical_data = (
      await axios.get(
        "https://api.coingecko.com/api/v3/coins/" +
          crypto_on_gecko.id +
          "/market_chart/range?vs_currency=" +
          currency +
          "&from=" +
          endDate.getTime() / 1000 +
          "&to=" +
          startDate.getTime() / 1000
      )
    ).data.prices;
    if (!historical_data || historical_data.length === 0) {
      return [];
    }
    coin_gecko_data = historical_data;
  }
  for (const holding of item.holdings) {
    var quote_rate = 0;
    if (holding.quote_rate === null || holding.quote_rate === 0) {
      let actual_date = new Date(holding.timestamp).getTime();
      coin_gecko_data.sort(function (a, b) {
        var distancea = Math.abs(actual_date - a[0]);
        var distanceb = Math.abs(actual_date - b[0]);
        return distancea - distanceb;
      });
      quote_rate = coin_gecko_data[0][1];
    } else {
      quote_rate = holding.quote_rate;
    }
    if (quote_rate === 0) {
      continue;
    }
    let value_usd =
      (holding.close.balance / Math.pow(10, item.contract_decimals)) *
      quote_rate;
    let date = new Date(holding.timestamp);
    let formatted_date =
      date.getFullYear() +
      "-" +
      ("0" + (date.getMonth() + 1)).slice(-2) +
      "-" +
      ("0" + date.getDate()).slice(-2);
    let index = data_returned.findIndex(
      (historical_object) => historical_object.time === formatted_date
    );
    if (index === -1) {
      data_returned.push({
        time: formatted_date,
        value: parseFloat(value_usd).toFixed(2),
        balance: holding.close.balance / Math.pow(10, item.contract_decimals),
      });
    } else {
      data_returned[index].value = (
        parseFloat(data_returned[index].value) + parseFloat(value_usd)
      ).toFixed(2);
    }
  }
  return data_returned.reverse();
}

function getTokenByContractAddress(tokens, contract) {
  return tokens.find(
    (obj) => obj.contract_address.toLowerCase() === contract.toLowerCase()
  );
}

async function getTransactionsBySymbol(
  mainnet_covalent_id,
  address,
  currency,
  symbol
) {
  if (symbol === "AVAX") {
    let tokens = await getERC20Tokens(mainnet_covalent_id);
    let transactions = await axios.get(
      "https://api.covalenthq.com/v1/" +
        mainnet_covalent_id +
        "/address/" +
        address +
        "/transactions_v2/?page-size=999999&key=" +
        process.env.REACT_APP_COVALENT_API_KEY
    );
    let data_returned = [];
    if (
      !transactions.data ||
      !transactions.data.data ||
      !transactions.data.data.items
    ) {
      return [];
    }
    for (const t of transactions.data.data.items) {
      var element_avax = {
        date: null,
        tx_id: null,
        type: null,
        amount: null,
        sender: null,
        contract_name: null,
        fees: null,
      };
      if (
        t.successful &&
        t.log_events.length > 0 &&
        t.log_events[0].decoded &&
        t.log_events[1]
      ) {
        var amount = null;
        element_avax.fees = (t.gas_spent*t.gas_price/10**18).toFixed(3);
        element_avax.tx_id = t.tx_hash;
        element_avax.date = t.block_signed_at;
        if (
          t.log_events[0].decoded.name === "Withdrawal" &&
          t.log_events[1].decoded.name === "Swap"
        ) {
          const token1 = getTokenByContractAddress(
            tokens.data.data.items,
            t.log_events[t.log_events.length - 1].sender_address
          );
          const token2 = getTokenByContractAddress(
            tokens.data.data.items,
            t.log_events[3].sender_address
          );
          if (token1.contract_name === "Wrapped AVAX") {
            element_avax.type = "OUT";
            element_avax.sender = token2.contract_address;
            element_avax.contract_name = token2.contract_name;
            if (t.log_events[1].decoded.params[1].value !== "0") {
              amount = (
                t.log_events[1].decoded.params[1].value /
                10 ** token1.contract_decimals
              ).toFixed(2);
            } else if (t.log_events[1].decoded.params[2].value !== "0") {
              amount = (
                t.log_events[1].decoded.params[2].value /
                10 ** token1.contract_decimals
              ).toFixed(2);
            }
            element_avax.amount = amount;
          } else {
            element_avax.sender = token1.contract_address;
            element_avax.contract_name = token1.contract_name;
            element_avax.type = "IN";
            if (t.log_events[1].decoded.params[1].value !== "0") {
              amount = (
                t.log_events[1].decoded.params[4].value /
                10 ** token2.contract_decimals
              ).toFixed(2);
            } else if (t.log_events[1].decoded.params[2].value !== "0") {
              amount = (
                t.log_events[1].decoded.params[3].value /
                10 ** token2.contract_decimals
              ).toFixed(2);
            }
            element_avax.amount = amount;
          }
          data_returned.push(element_avax);
        } else if (t.log_events[0].decoded.name === "Swap") {
          const token1 = getTokenByContractAddress(
            tokens.data.data.items,
            t.log_events[3].sender_address
          );
          const token2 = getTokenByContractAddress(
            tokens.data.data.items,
            t.log_events[2].sender_address
          );
          if (token1.contract_name === "Wrapped AVAX") {
            element_avax.type = "OUT";
            element_avax.sender = token2.contract_address;
            element_avax.contract_name = token2.contract_name;
            if (t.log_events[0].decoded.params[1].value !== "0") {
              amount = (
                t.log_events[0].decoded.params[1].value /
                10 ** token1.contract_decimals
              ).toFixed(2);
            } else if (t.log_events[0].decoded.params[2].value !== "0") {
              amount = (
                t.log_events[0].decoded.params[2].value /
                10 ** token1.contract_decimals
              ).toFixed(2);
            }
            element_avax.amount = amount;
          } else {
            element_avax.sender = token1.contract_address;
            element_avax.contract_name = token1.contract_name;
            element_avax.type = "IN";
            if (t.log_events[0].decoded.params[1].value !== "0") {
              amount = (
                t.log_events[0].decoded.params[4].value /
                10 ** token2.contract_decimals
              ).toFixed(2);
              if (amount === 0.0) {
                amount = (
                  t.log_events[0].decoded.params[3].value /
                  10 ** token2.contract_decimals
                ).toFixed(2);
              }
            } else if (t.log_events[0].decoded.params[2].value !== "0") {
              amount = (
                t.log_events[0].decoded.params[3].value /
                10 ** token2.contract_decimals
              ).toFixed(2);
            }
            element_avax.amount = amount;
          }
          data_returned.push(element_avax);
        }
      }
    }
    return data_returned;
  } else {
    let portfolio = (
      await axios.get(
        "https://api.covalenthq.com/v1/" +
          mainnet_covalent_id +
          "/address/" +
          address +
          "/balances_v2/?key=" +
          process.env.REACT_APP_COVALENT_API_KEY +
          "&nft=false&quote-currency=" +
          currency
      )
    ).data;
    if (portfolio.error) {
      return null;
    }
    let token = portfolio.data.items.find((tokenObject) => {
      return tokenObject.contract_ticker_symbol === symbol;
    });
    let transactions = await axios.get(
      "https://api.covalenthq.com/v1/" +
        mainnet_covalent_id +
        "/address/" +
        address +
        "/transfers_v2/?contract-address=" +
        token.contract_address +
        "&key=" +
        process.env.REACT_APP_COVALENT_API_KEY
    );
    let data_returned = [];
    if (
      !transactions.data ||
      !transactions.data.data ||
      !transactions.data.data.items
    ) {
      return [];
    }
    for (const transaction of transactions.data.data.items) {
      var element = {
        date: null,
        tx_id: null,
        type: null,
        amount: null,
        sender: null,
        contract_name: null,
        fees: null,
      };
      element.fees = (transaction.gas_spent*transaction.gas_price/10**18).toFixed(3);
      element.date = transaction.block_signed_at;
      element.tx_id = transaction.tx_hash;
      let transfer = transaction.transfers[0];
      element.type = transfer.transfer_type;
      element.contract_name = transfer.contract_name;
      if (element.type === "IN") {
        element.sender = transfer.from_address;
        element.amount =
          "+ " +
          (transfer.delta / Math.pow(10, transfer.contract_decimals)).toFixed(
            5
          );
      } else {
        element.sender = transfer.to_address;
        element.amount =
          "- " +
          (transfer.delta / Math.pow(10, transfer.contract_decimals)).toFixed(
            5
          );
      }
      data_returned.push(element);
    }
    return data_returned;
  }
}

async function getERC20Tokens(mainnet_covalent_id) {
  return await axios.get(
    "https://api.covalenthq.com/v1/" +
      mainnet_covalent_id +
      "/tokens/tokenlists/all/?page-size=9999999&key=" +
      process.env.REACT_APP_COVALENT_API_KEY
  );
}

export {
  getPortfolio,
  getGlobalValueOverTime,
  getValueOverTimeByToken,
  getTokenBySymbol,
  getTransactionsBySymbol,
  getNFTs,
};
