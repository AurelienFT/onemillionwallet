import { useState, useEffect } from "react";
import "./App.css";
import Mainnets from "./mainnets.json";
import Currencies from "./currencies.json";
import Transaction from "./transaction";
import NftMenu from "./nft_menu";
import { useHistory } from "react-router-dom";
import axios from "axios";

function App() {
  const [lastDarkMode, setLastDarkMode] = useState("no");
  const [darkMode, setDarkMode] = useState("no");
  const [currencyMenu, setCurrencyMenu] = useState(false);
  const [currencyValue, setCurrencyValue] = useState("");
  const [mainnetMenu, setMainnetMenu] = useState(false);
  const [mainnetName, setMainnetName] = useState("");
  const [isLogged, setLogged] = useState(null);
  const [addrError, setAddrError] = useState(false);

  let history = useHistory();

  useEffect(() => {
    var mainnet = parseInt(localStorage.getItem("mainnet"));
    if (
      !mainnet ||
      !Mainnets.find((mainnetObject) => mainnetObject.covalent_id === mainnet)
    ) {
      var mainnet_finded = Mainnets.find(
        (mainnetObject) => mainnetObject.name === "Avalanche"
      ).covalent_id;
      localStorage.setItem("mainnet", mainnet_finded);
      setMainnetName("Avalanche");
    } else {
      var net = Mainnets.find(
        (mainnetObject) => mainnetObject.covalent_id === mainnet
      );
      if (net) setMainnetName(net.name);
    }

    var currency = localStorage.getItem("currency");
    if (
      !currency ||
      !Currencies.find((currencyObject) => currencyObject.value === currency)
    ) {
      currency = Currencies.find(
        (currencyObject) => currencyObject.value === "USD"
      ).value;
      localStorage.setItem("currency", currency);
      setCurrencyValue(currency);
    } else {
      var cur = Currencies.find(
        (currencyObject) => currencyObject.value === currency
      );
      if (cur) {
        setCurrencyValue(cur.value);
      }
    }
    var address = localStorage.getItem("address");
    if (address && mainnetName !== "" && currencyValue !== "") {
      setLogged(address);
    }
    var darkmode = localStorage.getItem("darkmode");
    if (darkmode && mainnetName !== "" && currencyValue !== "") {
      setDarkMode(darkmode);
      setLastDarkMode(darkmode);
    } else if (!darkmode && mainnetName !== "" && currencyValue !== "") {
      localStorage.setItem("darkmode", "no");
      setLastDarkMode("no");
      setDarkMode("no");
    }
  }, [mainnetName, currencyValue]);

  function changeCurrency(currencyValueParam) {
    if (currencyValueParam === currencyValue) {
      return;
    }
    var currency = Currencies.find(
      (currencyObject) => currencyObject.value === currencyValueParam
    );
    if (currency) {
      localStorage.setItem("currency", currency.value);
      setCurrencyValue(currency.value);
      setCurrencyMenu(false);
    }
  }

  function changeMainnet(mainnetNameParam) {
    if (mainnetNameParam === mainnetName) {
      return;
    }
    var mainnet = Mainnets.find(
      (mainnetObject) => mainnetObject.name === mainnetNameParam
    );
    if (mainnet) {
      localStorage.setItem("mainnet", mainnet.covalent_id);
      setMainnetName(mainnetNameParam);
      setMainnetMenu(false);
    }
  }

  async function confirmAddress(e) {
    /*if (e.keyCode === 13) {
      await axios
        .get(
          "https://api.covalenthq.com/v1/1/address/" +
            e.target.value +
            "/balances_v2/?"
        )
        .then((resp) => {
          localStorage.setItem("address", e.target.value);
          setLogged(e.target.value);
          setAddrError(false);
        })
        .catch((err) => {
          console.log(err);
          setLogged(false);
          setAddrError(true);
        });*/
      localStorage.setItem("address", e.target.value);
      setLogged(e.target.value);
      console.log(isLogged);
  }

  return (
    <div className={darkMode === "yes" ? "dark" : ""}>
      <div
        onClick={() => {
          if (mainnetMenu) setMainnetMenu(false);
          if (currencyMenu) setCurrencyMenu(false);
        }}
        className="min-h-screen bg-auto bg-no-repeat bg-center bg-blue-400 dark:bg-gray-800"
      >
        <nav className="">
          <div className="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">
            <div className="relative flex items-center justify-between h-16">
              <div className="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
                <div className="flex-shrink-0 flex items-center">
                  <img
                    className="block lg:hidden h-8 w-auto"
                    src="https://uploads-ssl.webflow.com/605df6240893c6c5b2c7388e/60619b580d123b3c5432dc0f_Group%2013489.png"
                    alt="Workflow"
                  />
                  <img
                    className="hidden lg:block h-8 w-auto"
                    src="https://uploads-ssl.webflow.com/605df6240893c6c5b2c7388e/60619b580d123b3c5432dc0f_Group%2013489.png"
                    alt="Workflow"
                  />
                  <h2 className="text-xl font-bold leading-7 text-white ml-4 sm:truncate">
                    OneWillionWallet
                  </h2>
                </div>
              </div>
              <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
                {darkMode === "yes" ? (
                  <div className="flex justify-center items-center" onClick={() => {localStorage.setItem("darkmode", "no"); setDarkMode("no"); setLastDarkMode("yes");}}>
                    <span className="">
                      <svg
                        className="h-6 w-6 text-gray-400"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                      >
                        <path
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          stroke-width="2"
                          d="M12 3v1m0 16v1m9-9h-1M4 12H3m15.364 6.364l-.707-.707M6.343 6.343l-.707-.707m12.728 0l-.707.707M6.343 17.657l-.707.707M16 12a4 4 0 11-8 0 4 4 0 018 0z"
                        />
                      </svg>
                    </span>
                    <div className="w-14 h-7 flex items-center bg-gray-300 rounded-full mx-3 px-1 bg-gray-900">
                      <div className="bg-white w-5 h-5 rounded-full shadow-md transform translate-x-7"></div>
                    </div>
                    <span className="">
                      <svg
                        className="h-6 w-6 text-gray-500"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                      >
                        <path
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          stroke-width="2"
                          d="M20.354 15.354A9 9 0 018.646 3.646 9.003 9.003 0 0012 21a9.003 9.003 0 008.354-5.646z"
                        />
                      </svg>
                    </span>
                  </div>
                ) : (
                  <div className="flex justify-center items-center" onClick={() => {localStorage.setItem("darkmode", "yes"); setDarkMode("yes"); setLastDarkMode("no")}}>
                    <span className="">
                      <svg
                        className="h-6 w-6 text-gray-500"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                      >
                        <path
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          stroke-width="2"
                          d="M12 3v1m0 16v1m9-9h-1M4 12H3m15.364 6.364l-.707-.707M6.343 6.343l-.707-.707m12.728 0l-.707.707M6.343 17.657l-.707.707M16 12a4 4 0 11-8 0 4 4 0 018 0z"
                        />
                      </svg>
                    </span>
                    <div className="w-14 h-7 flex items-center bg-gray-300 rounded-full mx-3 px-1">
                      <div className="bg-white w-5 h-5 rounded-full shadow-md transform"></div>
                    </div>
                    <span className="">
                      <svg
                        className="h-6 w-6 text-gray-400"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                      >
                        <path
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          stroke-width="2"
                          d="M20.354 15.354A9 9 0 018.646 3.646 9.003 9.003 0 0012 21a9.003 9.003 0 008.354-5.646z"
                        />
                      </svg>
                    </span>
                  </div>
                )}
                <div className="relative ml-3">
                  <button
                    type="button"
                    className="text-white group inline-flex items-center text-base font-medium hover:text-gray-500"
                    onClick={() => {
                      setCurrencyMenu(!currencyMenu);
                    }}
                    aria-expanded="false"
                  >
                    <span>{currencyValue}</span>
                    <svg
                      className="text-white ml-2 h-5 w-5 group-hover:text-gray-500"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                      aria-hidden="true"
                    >
                      <path
                        fillRule="evenodd"
                        d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                        clipRule="evenodd"
                      />
                    </svg>
                  </button>
                  {currencyMenu ? (
                    <div className="absolute z-10 ml-4 transform w-20 lg:left-1/4 lg:-translate-x-1/2">
                      <div className="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 overflow-hidden">
                        <div className="relative grid bg-white px-5 py-2">
                          <div className="ml-1 grid grid-cols-1 divide-y">
                            <button
                              onClick={() => {
                                changeCurrency("USD");
                              }}
                              className="text-base font-medium text-gray-900"
                            >
                              USD
                            </button>
                            <button
                              onClick={() => {
                                changeCurrency("CAD");
                              }}
                              className="text-base font-medium text-gray-900"
                            >
                              CAD
                            </button>
                            <button
                              onClick={() => {
                                changeCurrency("EUR");
                              }}
                              className="text-base font-medium text-gray-900"
                            >
                              EUR
                            </button>
                            <button
                              onClick={() => {
                                changeCurrency("SGD");
                              }}
                              className="text-base font-medium text-gray-900"
                            >
                              SGD
                            </button>
                            <button
                              onClick={() => {
                                changeCurrency("INR");
                              }}
                              className="text-base font-medium text-gray-900"
                            >
                              INR
                            </button>
                            <button
                              onClick={() => {
                                changeCurrency("JPY");
                              }}
                              className="text-base font-medium text-gray-900"
                            >
                              JPY
                            </button>
                            <button
                              onClick={() => {
                                changeCurrency("VND");
                              }}
                              className="text-base font-medium text-gray-900"
                            >
                              VND
                            </button>
                            <button
                              onClick={() => {
                                changeCurrency("CNY");
                              }}
                              className="text-base font-medium text-gray-900"
                            >
                              CNY
                            </button>
                            <button
                              onClick={() => {
                                changeCurrency("KRW");
                              }}
                              className="text-base font-medium text-gray-900"
                            >
                              KRW
                            </button>
                            <button
                              onClick={() => {
                                changeCurrency("RUB");
                              }}
                              className="text-base font-medium text-gray-900"
                            >
                              RUB
                            </button>
                            <button
                              onClick={() => {
                                changeCurrency("TRY");
                              }}
                              className="text-base font-medium text-gray-900"
                            >
                              TRY
                            </button>
                            <button
                              onClick={() => {
                                changeCurrency("ETH");
                              }}
                              className="text-base font-medium text-gray-900"
                            >
                              ETH
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  ) : (
                    <div></div>
                  )}
                </div>
                <div className="relative ml-3">
                  <button
                    type="button"
                    className="text-white group inline-flex items-center text-base font-medium hover:text-gray-500"
                    onClick={() => {
                      setMainnetMenu(!mainnetMenu);
                    }}
                    aria-expanded="false"
                  >
                    <span>{mainnetName}</span>
                    <svg
                      className="text-white ml-2 h-5 w-5 group-hover:text-gray-500"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                      aria-hidden="true"
                    >
                      <path
                        fillRule="evenodd"
                        d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                        clipRule="evenodd"
                      />
                    </svg>
                  </button>
                  {mainnetMenu ? (
                    <div className="absolute z-10 -ml-4 transform w-80 lg:left-1/2 lg:-translate-x-1/2">
                      <div className="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 overflow-hidden">
                        <div className="relative grid bg-white px-5 py-6">
                          <div className="ml-4 grid grid-cols-1 divide-y">
                            <button
                              onClick={() => {
                                changeMainnet("Avalanche");
                              }}
                              className="text-base font-medium text-gray-900"
                            >
                              Avalanche
                            </button>
                            <button
                              onClick={() => {
                                changeMainnet("Avalanche FUJI");
                              }}
                              className="text-base font-medium text-gray-900"
                            >
                              Avalanche FUJI
                            </button>
                            <button
                              onClick={() => {
                                changeMainnet("Ethereum");
                              }}
                              className="text-base font-medium text-gray-900"
                            >
                              Ethereum
                            </button>
                            <button
                              onClick={() => {
                                changeMainnet("Binance Smart Chain");
                              }}
                              className="text-base font-medium text-gray-900"
                            >
                              Binance Smart Chain
                            </button>
                            <button
                              onClick={() => {
                                changeMainnet("Polygon/Matic");
                              }}
                              className="text-base font-medium text-gray-900"
                            >
                              Polygon/Matic
                            </button>
                            <button
                              onClick={() => {
                                changeMainnet("Polygon/Matic Mumbai Testnet");
                              }}
                              className="text-base font-medium text-gray-900"
                            >
                              Polygon/Matic Mumbai Testnet
                            </button>
                            <button
                              onClick={() => {
                                changeMainnet("Fantom Opera Mainnet");
                              }}
                              className="text-base font-medium text-gray-900"
                            >
                              Fantom Opera Mainnet
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  ) : (
                    <div></div>
                  )}
                </div>
                <div className="ml-3 relative">
                  <input
                    id="address_header"
                    name="address_header"
                    type="text"
                    onKeyDown={confirmAddress}
                    required
                    className="appearance-none rounded relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm dark:bg-gray-300 dark:text-white"
                    placeholder="Enter your address"
                  />
                </div>
              </div>
            </div>
          </div>
        </nav>
        <div>
          {isLogged ? (
            <div>
              <Transaction
                mainnetName={mainnetName}
                currency={currencyValue}
                address={isLogged}
                reload={!mainnetMenu && !currencyMenu && lastDarkMode === darkMode}
                darkMode={darkMode}
              ></Transaction>
              <NftMenu
                mainnetName={mainnetName}
                address={isLogged}
                reload={!mainnetMenu && !currencyMenu && lastDarkMode === darkMode}
              ></NftMenu>
            </div>
          ) : (
            <div className="overflow-x-auto">
              <div className="min-w-screen min-h-screen flex items-center justify-center overflow-hidden bg-auto bg-no-repeat bg-center bg-gradient-to-b from-blue-500 to-blue-100 ...">
                <div className="w-full lg:w-1/6">
                  <input
                    type="text"
                    name="address"
                    id="address"
                    onKeyDown={confirmAddress}
                    className="appearance-none rounded relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                    placeholder="Your address"
                  />
                </div>
                {addrError ? (
                  <div
                    className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                    role="alert"
                  >
                    <strong className="font-bold">Error Wrong adress</strong>
                    {/* <span className="block sm:inline">Something seriously bad happened.</span> */}
                    <span className="absolute top-0 bottom-0 right-0 px-4 py-3">
                      <svg
                        className="fill-current h-6 w-6 text-red-500"
                        role="button"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 20 20"
                      >
                        <title>Close</title>
                        <path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z" />
                      </svg>
                    </span>
                  </div>
                ) : (
                  <div></div>
                )}
              </div>
            </div>
          )}
        </div>
        <div className=" bg-blue-500 dark:bg-gray-900 ">
          <footer className="flex flex-wrap items-center justify-between p-3 m-auto">
            <div className="container mx-auto flex flex-col flex-wrap items-center justify-between">
              <div className="flex mx-auto text-white text-center">
                Made for OneMillionWallet Hackathon by Aurélien FOUCAULT and Alexandre SANANIKONE.
              </div>
              <div className="flex mx-auto text-white text-center">
                Powered by Covalent and CoinGecko
              </div>
              <button
                type="button"
                className="inline-flex items-center px-4 py-2 border border-purple-400 text-base leading-6 font-medium rounded-md text-purple-800 bg-white hover:text-purple-700 focus:border-purple-300 transition ease-in-out duration-150"
                onClick={() => history.push("/nft_support")}
              >
                Supported NFTs
              </button>
              <div className="flex mx-auto text-white text-center">
                Tips are appreciated to finance the server :
                0x665342762FDD54D0303c195fec3ce2568b62052e
              </div>
            </div>
          </footer>
        </div>
      </div>
    </div>
  );
}

export default App;
